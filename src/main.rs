use lambda_extension::tracing;
use otlp_extension::error::OtlpExtensionError;

#[tokio::main]
async fn main() -> Result<(), OtlpExtensionError> {
    tracing::init_default_subscriber();

    otlp_extension::run_extension().await?;
    Ok(())
}
