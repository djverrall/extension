use thiserror::Error;

#[derive(Error, Debug)]
pub enum OtlpExtensionError {
    #[error("An error occurred whilst handling a lambda event")]
    ExtensionError(#[from] lambda_extension::Error),
    #[error("An error occurred in the Axum server")]
    AxumError(#[from] std::io::Error),
    #[error("Error while handling a lambda invoke or shutdown event")]
    ExtensionEventError,
}
