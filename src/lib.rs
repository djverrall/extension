pub mod error;
mod extension;
mod server;
mod telemetry_processor;

use crate::error::OtlpExtensionError;
use lambda_extension::tracing;
use tokio_util::sync::CancellationToken;

pub async fn run_extension() -> Result<(), OtlpExtensionError> {
    let axum_token = CancellationToken::new();
    let lambda_token = CancellationToken::new();

    let (extension, server) = tokio::join!(
        extension::run_extension(axum_token.clone(), lambda_token.clone()),
        server::run_server(axum_token.clone(), lambda_token.clone())
    );

    if let Err(e) = extension {
        tracing::error!(error = ?e, "extension failed");
        return Err(e);
    }
    if let Err(e) = server {
        tracing::error!(error = ?e, "server failed");
        return Err(e);
    }

    Ok(())
}
