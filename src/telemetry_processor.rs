/// Adheres to the guidelines setout for Lambda at https://opentelemetry.io/docs/specs/semconv/faas/aws-lambda/
use chrono::{DateTime, Utc};
use lambda_extension::tracing::info;
use lambda_extension::{LambdaEvent, LambdaTelemetry, Service};
use opentelemetry::trace::Status;
use std::thread;

use std::{
    future::{ready, Future},
    pin::Pin,
    sync::{Arc, Mutex},
};

use serde::{Deserialize, Serialize};
use tokio_util::sync::CancellationToken;

use crate::error::OtlpExtensionError;
use opentelemetry::KeyValue;
use opentelemetry_semantic_conventions::{resource, trace};

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
enum Trigger {
    Datasource,
    Http,
    Pubsub,
    Timer,
    #[default]
    Other,
}

#[derive(Debug, Clone)]
pub struct LambdaOtlpExtension {
    tracing: Arc<Mutex<HandlerTracing>>,
    cancellation_token: CancellationToken,
}

#[derive(Debug, Default)]
pub struct HandlerTracing {
    init_start_time: Option<DateTime<Utc>>,
    init_end_time: Option<DateTime<Utc>>,
    init_status: Option<Status>,
    init_children: Vec<lambda_extension::Span>,
    invoke_start_time: Option<DateTime<Utc>>,
    invoke_end_time: Option<DateTime<Utc>>,
    invoke_children: Vec<lambda_extension::Span>,
    invoke_status: Option<Status>,
    invocation_attrs: Vec<KeyValue>,
    resource_attrs: Vec<KeyValue>,
}

impl Service<LambdaEvent> for LambdaOtlpExtension {
    type Response = ();

    type Error = OtlpExtensionError;

    type Future = Pin<Box<dyn Future<Output = Result<(), OtlpExtensionError>> + Send>>;

    fn poll_ready(
        &mut self,
        _cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        core::task::Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: LambdaEvent) -> Self::Future {
        match req.next {
            lambda_extension::NextEvent::Invoke(e) => {
                let c_mutex = Arc::clone(&self.tracing);

                thread::spawn(move || {
                    c_mutex.lock().unwrap().invocation_attrs.extend(vec![
                        KeyValue::new(trace::FAAS_INVOCATION_ID, e.request_id),
                        KeyValue::new(trace::AWS_LAMBDA_INVOKED_ARN, e.invoked_function_arn),
                    ]);
                })
                .join()
                .expect("thread::spawn failed");
            }
            lambda_extension::NextEvent::Shutdown(e) => {
                info!("Shutdown event: {:?}", e);
                self.cancellation_token.cancel();
            }
        }
        Box::pin(ready(Ok(())))
    }
}

impl Service<Vec<LambdaTelemetry>> for LambdaOtlpExtension {
    type Response = ();

    type Error = OtlpExtensionError;

    type Future = Pin<Box<dyn Future<Output = Result<(), OtlpExtensionError>> + Send>>;

    fn poll_ready(
        &mut self,
        _cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        core::task::Poll::Ready(Ok(()))
    }

    fn call(&mut self, events: Vec<LambdaTelemetry>) -> Self::Future {
        for event in events {
            match event.record {
                lambda_extension::LambdaTelemetryRecord::PlatformInitStart {
                    initialization_type,
                    phase,
                    runtime_version,
                    runtime_version_arn,
                } => {
                    let c_mutex = Arc::clone(&self.tracing);
                    let initialization_type = match initialization_type {
                        lambda_extension::InitType::OnDemand => "on_demand",
                        lambda_extension::InitType::ProvisionedConcurrency => {
                            "provisioned_concurrency"
                        }
                        lambda_extension::InitType::SnapStart => "snap_start",
                    };
                    thread::spawn(move || {
                        c_mutex.lock().unwrap().init_start_time = Some(event.time);
                        c_mutex.lock().unwrap().resource_attrs.extend(vec![
                            KeyValue::new(
                                resource::PROCESS_RUNTIME_DESCRIPTION,
                                runtime_version_arn.unwrap_or("unspecified".into()),
                            ),
                            KeyValue::new(
                                resource::PROCESS_RUNTIME_VERSION,
                                runtime_version.unwrap_or("unspecified".into()),
                            ),
                            KeyValue::new(
                                "faas.initialization_type",
                                initialization_type.to_string(),
                            ),
                        ]);
                        match phase {
                            lambda_extension::InitPhase::Init => c_mutex
                                .lock()
                                .unwrap()
                                .invocation_attrs
                                .extend(vec![KeyValue::new(trace::FAAS_COLDSTART, true)]),
                            lambda_extension::InitPhase::Invoke => {}
                        };
                    })
                    .join()
                    .expect("thread::spawn failed for PlatformInitStart");
                }
                lambda_extension::LambdaTelemetryRecord::PlatformInitRuntimeDone {
                    status,
                    error_type,
                    spans,
                    ..
                } => {
                    let status = match status {
                        lambda_extension::Status::Success => Status::Unset,
                        lambda_extension::Status::Error => {
                            let error_type = error_type.unwrap_or("Error".into());
                            Status::error(error_type)
                        }
                        lambda_extension::Status::Failure => {
                            let error_type = error_type.unwrap_or("Failure".into());
                            Status::error(error_type.clone())
                        }
                        lambda_extension::Status::Timeout => {
                            let error_type = error_type.unwrap_or("Timeout".into());
                            Status::error(error_type.clone())
                        }
                    };
                    let c_mutex = Arc::clone(&self.tracing);
                    thread::spawn(move || {
                        c_mutex.lock().unwrap().init_end_time = Some(event.time);
                        c_mutex.lock().unwrap().init_status = Some(status);
                        c_mutex.lock().unwrap().init_children.extend(spans);
                    });
                }
                lambda_extension::LambdaTelemetryRecord::PlatformInitReport {
                    metrics,
                    spans,
                    ..
                } => {
                    let c_mutex = Arc::clone(&self.tracing);
                    thread::spawn(move || {
                        c_mutex.lock().unwrap().init_children.extend(spans);
                        c_mutex
                            .lock()
                            .unwrap()
                            .invocation_attrs
                            .extend(vec![KeyValue::new(
                                "faas.init_duration",
                                metrics.duration_ms,
                            )]);
                    });
                }
                lambda_extension::LambdaTelemetryRecord::PlatformStart { version, .. } => {
                    let c_mutex = Arc::clone(&self.tracing);

                    thread::spawn(move || {
                        c_mutex.lock().unwrap().invoke_start_time = Some(event.time);
                        c_mutex
                            .lock()
                            .unwrap()
                            .resource_attrs
                            .extend(vec![KeyValue::new(
                                resource::FAAS_VERSION,
                                version.unwrap_or("unspecified".into()),
                            )])
                    });
                }
                lambda_extension::LambdaTelemetryRecord::PlatformRuntimeDone {
                    status,
                    error_type,
                    metrics,
                    spans,
                    ..
                } => {
                    let status = match status {
                        lambda_extension::Status::Success => Status::Unset,
                        lambda_extension::Status::Error => {
                            Status::error(error_type.unwrap_or("Error".into()))
                        }
                        lambda_extension::Status::Failure => {
                            Status::error(error_type.unwrap_or("Failure".into()))
                        }
                        lambda_extension::Status::Timeout => {
                            Status::error(error_type.unwrap_or("Timeout".into()))
                        }
                    };

                    let trace_attrs = match metrics {
                        Some(metrics) => {
                            let produced_bytes = match metrics.produced_bytes {
                                Some(bytes) => {
                                    // Ensure that the value is within the bounds of i64
                                    let bytes = bytes.min(i64::MAX as u64) as i64;
                                    KeyValue::new("faas.invocation_produced_bytes", bytes)
                                }
                                None => {
                                    KeyValue::new("faas.invocation_produced_bytes", "unspecified")
                                }
                            };
                            vec![
                                KeyValue::new("faas.invoke_duration", metrics.duration_ms),
                                produced_bytes,
                            ]
                        }
                        None => vec![],
                    };

                    let c_mutex = Arc::clone(&self.tracing);
                    thread::spawn(move || {
                        c_mutex.lock().unwrap().invoke_end_time = Some(event.time);
                        c_mutex.lock().unwrap().invoke_status = Some(status);
                        c_mutex.lock().unwrap().invoke_children.extend(spans);
                        c_mutex.lock().unwrap().invocation_attrs.extend(trace_attrs);
                    });
                }
                lambda_extension::LambdaTelemetryRecord::PlatformReport {
                    metrics,
                    spans,
                    ..
                } => {
                    // When we match on this branch the invocation is complete and we should create and
                    // send the spans to the OTLP collector.
                    let mut report_attrs = vec![
                        KeyValue::new("faas.billed_duration_ms", metrics.billed_duration_ms as i64),
                        KeyValue::new("faas.max_memory_used_mb", metrics.max_memory_used_mb as i64),
                        KeyValue::new("faas.duration_ms", metrics.duration_ms),
                        KeyValue::new("faas.memory_size_mb", metrics.memory_size_mb as i64),
                    ];
                    if let Some(init_duration) = metrics.init_duration_ms {
                        report_attrs.push(KeyValue::new("faas.init_duration_ms", init_duration));
                    }
                    let c_mutex = Arc::clone(&self.tracing);
                    thread::spawn(move || {
                        c_mutex.lock().unwrap().invoke_children.extend(spans);
                        c_mutex.lock().unwrap().invocation_attrs.extend(report_attrs);
                    });
                    self.build_and_send_traces();
                },
                _ => {}
            }
        }
        Box::pin(ready(Ok(())))
    }
}

impl LambdaOtlpExtension {
    pub fn new(tracing: Arc<Mutex<HandlerTracing>>, cancellation_token: CancellationToken) -> Self {
        LambdaOtlpExtension {
            tracing,
            cancellation_token,
        }
    }
    fn build_and_send_traces(&self) {
        todo!("Implement this function")
    }
}
