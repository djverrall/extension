use std::sync::{Arc, Mutex};

use crate::{error::OtlpExtensionError, telemetry_processor};
use anyhow::Result;

use lambda_extension::{Extension, SharedService};

use tokio_util::sync::CancellationToken;

pub async fn run_extension(
    axum_token: CancellationToken,
    lambda_token: CancellationToken,
) -> Result<(), OtlpExtensionError> {
    let tracing = Arc::new(Mutex::new(telemetry_processor::HandlerTracing::default()));
    let ep = telemetry_processor::LambdaOtlpExtension::new(tracing.clone(), lambda_token.clone());
    let tp = SharedService::new(telemetry_processor::LambdaOtlpExtension::new(
        tracing.clone(),
        lambda_token.clone(),
    ));

    let extension = Extension::new()
        .with_telemetry_types(&["platform"])
        .with_events_processor(ep)
        .with_telemetry_processor(tp)
        .run();

    tokio::select! {
        _ = extension => {
            lambda_extension::tracing::info!("extension was shutdown");
            Ok(())
        }
        _ = axum_token.cancelled() => {
            lambda_extension::tracing::info!("axum server was shutdown");
            Ok(())
        }
    }
}
