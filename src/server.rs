use tokio_util::sync::CancellationToken;

use crate::error::OtlpExtensionError;

use axum::{routing::post, Router};
use tokio::signal;

pub async fn graceful_shutdown(axum_token: CancellationToken, lambda_token: CancellationToken) {
    let sigterm = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .expect("failed to register SIGTERM handler")
            .recv()
            .await;
    };

    tokio::select! {
        _ = sigterm => {
            lambda_extension::tracing::info!("received SIGTERM");
            axum_token.cancel();
        }
        _ = lambda_token.cancelled() => {
            lambda_extension::tracing::info!("lambda extension was shutdown");
            axum_token.cancel();
        }
    }
}

pub async fn run_server(
    axum_token: CancellationToken,
    lambda_token: CancellationToken,
) -> Result<(), OtlpExtensionError> {
    let app = Router::new().route("/v1/:signal", post(|| async { "Hello, World!" }));

    let listener = tokio::net::TcpListener::bind("127.0.0.1:4318").await?;
    let _server = axum::serve(listener, app)
        .with_graceful_shutdown(graceful_shutdown(axum_token, lambda_token))
        .await;

    Ok(())
}
